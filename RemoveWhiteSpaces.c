
//This programm deletes all whitespaces in a file, allignment remains

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#define LINE_SIZE 200
                       
int print_fixed_buffer(char* buffer){

	int i=0;

	while(buffer[i] == ' ')
		i++;
	
	if (buffer[i] == '\n'){
		printf("\n");
		return i;
	}
	printf("%s",buffer);

	return 0;
}

int main(int argc, char* argv[]){

	if( argc != 2){
		printf("define source file name\n");
		return 0;
	}

	FILE *fp;
	char* file = argv[1];

	if (strcmp("RemoveWhiteSpaces.c" , argv[1]) == 0)
		fprintf(stderr, "*%s*\n", file);

	if ( (fp = fopen( argv[1] , "r")) == NULL){
		perror("open");
		return -1;
	}

	char ch;
	int whitespaces=0;	
	char* buffer = malloc(LINE_SIZE);
	
	if (buffer == NULL){
		perror("malloc");
		return -1;
	}

	memset(buffer, 0 , LINE_SIZE);

	while( fgets(buffer,LINE_SIZE,fp) ){
		
		whitespaces += print_fixed_buffer(buffer);
		memset(buffer, 0 , LINE_SIZE);	
	}

	fprintf(stderr,"Total whitespaces:%d\n", whitespaces); 	
	
	free(buffer);

	if (fclose(fp)<0){
		perror("fclose");
		return -1;
	}

	return 0;	
}
